const std = @import("std");
const c = @import("c.zig");

const Allocator = std.mem.Allocator;
const Window = @import("Window.zig");
const TileRenderer = @import("TileRenderer.zig");

const Self = @This();

window: Window,
renderer: TileRenderer,
allocator: *Allocator,

quit: bool = false,
t: u8 = 0,

pub fn init(title: [:0]const u8, width: u16, height: u16, allocator: *Allocator) !Self {
    const window = try Window.init(title, width * 12, height * 12);
    const renderer = try TileRenderer.init(width, height, 2, "data/tileset", allocator);

    return Self{
        .window = window,
        .renderer = renderer,
        .allocator = allocator,
    };
}

pub fn deinit(self: *Self) void {
    self.renderer.deinit();
    self.window.deinit();
    self.* = undefined;
}

pub fn mainLoop(self: *Self) !void {
    while (!self.quit) {
        try self.handleEvents();
        try self.update();
        try self.draw();
    }
}

pub fn handleEvents(self: *Self) !void {
    var event: c.SDL_Event = undefined;
    while (c.SDL_PollEvent(&event) != 0) {
        switch (event.type) {
            c.SDL_QUIT => self.quit = true,
            c.SDL_WINDOWEVENT => switch (event.window.event) {
                c.SDL_WINDOWEVENT_RESIZED => self.resize(event.window.data1, event.window.data2),
                else => {},
            },
            else => {},
        }
    }
}

pub fn update(self: *Self) !void {
    try self.renderer.updateCell(0, 0, 0, self.t, .{ 255, 127, 255 });
    try self.renderer.updateCell(39, 24, 1, 255 - self.t, .{ 255, 127, 127 });
    self.t +%= 1;
}

pub fn draw(self: *Self) !void {
    c.glClearColor(0.0, 0.0, 0.0, 1.0);
    c.glClear(c.GL_COLOR_BUFFER_BIT);

    self.renderer.draw();

    c.SDL_GL_SwapWindow(self.window.win);
}

fn resize(self: *Self, width: c_int, height: c_int) void {
    _ = self;
    c.glViewport(0, 0, width, height);
}
