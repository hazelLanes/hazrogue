const std = @import("std");
const c = @import("c.zig");

const Self = @This();

const WindowError = error{
    InitFailed,
    WindowNotCreated,
    ContextNotCreated,
    GlNotLoaded,
};

win: *c.SDL_Window,
ctx: c.SDL_GLContext,

pub fn init(title: [:0]const u8, width: c_int, height: c_int) !Self {
    // Initialize SDL
    if (c.SDL_Init(c.SDL_INIT_VIDEO) < 0) {
        return error.InitFailed;
    }
    errdefer c.SDL_Quit();

    // Create window
    const win = c.SDL_CreateWindow(title, c.SDL_WINDOWPOS_CENTERED, c.SDL_WINDOWPOS_CENTERED, width, height, c.SDL_WINDOW_OPENGL | c.SDL_WINDOW_RESIZABLE);
    if (win == null) {
        std.log.err("Window could not be created: {s}", .{c.SDL_GetError()});
        return error.WindowNotCreated;
    }
    errdefer c.SDL_DestroyWindow(win);

    // Set OpenGL context hints
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_DOUBLEBUFFER, 1);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_MINOR_VERSION, 3);
    _ = c.SDL_GL_SetAttribute(c.SDL_GL_CONTEXT_PROFILE_MASK, c.SDL_GL_CONTEXT_PROFILE_CORE);

    // Create OpenGL context
    var ctx = c.SDL_GL_CreateContext(win);
    if (ctx == null) {
        std.log.err("OpenGL context could not be created: {s}", .{c.SDL_GetError()});
        return error.ContextNotCreated;
    }
    errdefer c.SDL_GL_DeleteContext(ctx);

    // Enable vsync
    if (c.SDL_GL_SetSwapInterval(1) < 0) {
        std.log.warn("Could not enable vsync for some fucking reason. Here is the error I guess: {s}", .{c.SDL_GetError()});
    }

    // Load and set up OpenGL with GLAD
    if (c.gladLoadGL(@ptrCast(c.GLADloadfunc, c.SDL_GL_GetProcAddress)) == 0) {
        return error.GlNotLoaded;
    }

    // Disable depth tests and culling because we will never need them in this engine
    c.glDisable(c.GL_DEPTH_TEST);
    c.glDisable(c.GL_CULL_FACE);

    return Self{
        .win = win.?,
        .ctx = ctx,
    };
}

pub fn deinit(self: *Self) void {
    c.SDL_GL_DeleteContext(self.ctx);
    c.SDL_DestroyWindow(self.win);
    c.SDL_Quit();

    self.* = undefined;
}
